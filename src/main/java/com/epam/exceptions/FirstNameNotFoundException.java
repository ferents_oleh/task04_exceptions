package com.epam.exceptions;

public class FirstNameNotFoundException extends Exception {
    public FirstNameNotFoundException(String message) {
        super(message);
    }
}
