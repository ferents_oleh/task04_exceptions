package com.epam.model.dao;

import com.epam.exceptions.FirstNameNotFoundException;
import com.epam.model.domain.User;

import java.util.List;

public interface UserDao {
    List<User> getAllUsers();

    List<User> getAllUsersByFirstName(String firstName) throws FirstNameNotFoundException;

    void addUser(User user);
}
