package com.epam;

import com.epam.controller.UserController;
import com.epam.controller.impl.UserControllerImpl;
import com.epam.model.dao.UserDao;
import com.epam.model.dao.impl.UserDaoImpl;
import com.epam.view.Menu;

public class Application {
    public static void main(String[] args) {
        UserDao userDao = new UserDaoImpl();
        UserController userController = new UserControllerImpl(userDao);

        new Menu(userController);
    }
}
